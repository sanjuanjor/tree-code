#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "utility.h"

// needed for indexx to short the array

#define NAME_OF_FILE "sph.dat"
#define N_PARTS 1500
#define N_STEPTS 200
#define N_SPH 100
#define T_END 0.1
#define T_START 0.
#define M 1.
#define g 7./5.


struct particle {
  double m;
  double x;	
  double  v;	
  double rho;	
  double e;	
  double  h;	
  double  v_coeff;	
  double  rho_coeff;	
  double  e_coeff;	

};


typedef struct particle particle_t;  // typedef (WHAT_YOU_WANT_TO_AVOID) (SHORTCUT)
double distance(double x1, double x2);
double DWij(double h , double xi ,double xj);
void print_to_a_file(FILE *fp,particle_t *P, int step);
  




void main()
{
  int i,j,jj,ii;
  FILE *fp;
  particle_t *P;
  double *d,prefactor;
  long *index;
  prefactor=(g-1);
  double dx=1./(double)(N_PARTS-1);
  double dt=(double)(T_END-T_START)/(double)(N_STEPTS);
  fp=fopen(NAME_OF_FILE,"w");
  //  printf("%f \n",dt);
  double wij;
  

  P = (particle_t *) calloc(N_PARTS, sizeof(particle_t));

  // to make it parallel i need to use this again
  index=(long*) calloc(N_PARTS,sizeof(long));	
  d = (double *) calloc(N_PARTS, sizeof(double));
    
  // set initial conditions //
  //#pragma omp parallel private(i) shared(P,dx) default(none)
  for(i=0;i<N_PARTS;i++)
      
      {
        (P+i)->m=M;
        (P+i)->x=dx*i;
        (P+i)->v=0.;
        (P+i)->rho=M/dx;
        (P+i)->e=1.e-5;
        (P+i)->h=0.;
        (P+i)->v_coeff=0.;
        (P+i)->rho_coeff=0.;
        (P+i)->e_coeff=0.;
      }
    //change e of the midel elemnt to 1
  (P+(int)(floor(N_PARTS/2.)))->e=1.;
  printf("%f \n" , (P+7)->x);
    
    for (jj=0;jj<N_STEPTS;jj++){
      // set h and sph coefficients 
      //#pragma omp parallel private(i,d,wij,ii,j,index) shared(P,prefactor) default(none) //schedule(dynamic)
      for(i=0;i<N_PARTS;i++)
        {
          //  index=(long*) calloc(N_PARTS,sizeof(long));	
          //d = (double *) calloc(N_PARTS, sizeof(double));

          // make distance square vector 
          //#pragma omp parallel private(j) shared(P,i,d) default(none)
          for (j=0;j<N_PARTS;j++)
            d[j]=distance(P[j].x,P[i].x);
          indexx((long)N_PARTS,d-1,index-1);
          //        h=distance((P+index[N_SPH]-1),(P+i));
          
          (P+i)->h=distance(P[index[N_SPH]-1].x,P[i].x);
          
          // printf("%f \n",(P+i)->h);
          (P+i)->v_coeff=0.;
          (P+i)->rho_coeff=0.;
          (P+i)->e_coeff=0.;

        // set coefficents to 0
        
          
          /* for (j=1;j<N_PARTS;j++) */
          /*   { // we start at 1 so we dont take itself into consideration and we end later so we have the n neighbours */
          /*   // P[index[j]-1]; */
          /*   //printf("%f \n",DWij( (P+i)->h , (P+i)->x , (P+index[j]-1)->x) ); */
          /*     ii=index[j]-1; */
          /*     wij=DWij( (P+i)->h , (P+i)->x , (P+ii)->x); */
          /*     if (wij==0) */
          /*       break; */
          /*     else{ */
          /*       (P+i)->v_coeff -= (P+ii)->m * prefactor * ((P+ii)->e / (P+ii)->rho  + (P+i)->e / (P+i)->rho ) * wij; */
          /*       (P+i)->rho_coeff += (P+ii)->m*( (P+i)->v  -  (P+ii)->v )*wij; */
          /*       (P+i)->e_coeff += 0.5 * (P+ii)->m*prefactor*((P+ii)->e/(P+ii)->rho  + (P+i)->e/(P+i)->rho )*((P+i)->v  -  (P+ii)->v )*wij; */
          /*     }  */
          /*   } */
          /* //          free(d); */
          /* // free(index); */

          //        }

      // we make two options one just taking the particles in order of distance and breaking the loop as soon as we get our first zero in wij maybe better for many particles
      //the other options is just going throw the loop with i !=j and suming all the zeros you encounter  maybe performance benefict if many particles 
      for (j=0;j<N_PARTS;j++){               
              if(i!=j){
                P[i].v_coeff -= prefactor*(P+j)->m * (((P+j)->e / (P+j)->rho)  + ((P+i)->e / (P+i)->rho )) * DWij( (P+i)->h , (P+i)->x , (P+j)->x) ;
                P[i].rho_coeff += (P+j)->m *( (P+i)->v  -  (P+j)->v )*DWij( (P+i)->h, (P+i)->x , (P+j)->x );
                P[i].e_coeff += 0.5 * prefactor*(P+j)->m *(((P+j)->e/(P+j)->rho ) +( (P+i)->e/(P+i)->rho) )*((P+i)->v  -  (P+j)->v )*DWij( (P+i)->h, (P+i)->x , (P+j)->x );
            }
            }

      
      }

      //
          //printf("%f \n",(P+i)->rho_coeff);
        

      /// now time integration
      
      for(i=0;i<N_PARTS;i++){
        (P+i)->x += ((P+i)->v *dt);
        (P+i)->v += ((P+i)->v_coeff *dt);
        (P+i)->rho += ((P+i)->rho_coeff *dt);
        (P+i)->e += ((P+i)->e_coeff *dt);  
      }
      
      /// print to a file or something

      print_to_a_file(fp,P,jj);
    }

      free(P);
      free(d);
      free(index);

      fclose(fp);

      
    }
      
    

double DWij(double h , double xi ,double xj)
{
  double r;
  r=fabs(xi-xj);
  if( r <= h)
    return (SIGN(xi-xj))*((2./(3.*h))*((-3.*r / (h*h)) +(( 9./4.) *( r * r) /(h*h*h)))); 
  else if ( r <= 2.*h)
    return (SIGN(xi-xj))*((2./(3.*h))*((3./4.)*(2.-(r/h))*(2.-(r/h))*(-1./h)));
  else
    return 0.;
}

double distance(double x1, double x2)
{
  return fabs(x1-x2);
    

}
void print_to_a_file(FILE *fp,particle_t *P,int step)
{
  int i;
  fprintf(fp,"\n \n  # %d step \n",step);
  for(i=0;i<N_PARTS;i++){
  //        printf("%f %f\n",*(c_real+i),*(c_img+j));
    fprintf(fp,"%f %f\n",(P+i)->rho,(P+i)->x);
}
}










