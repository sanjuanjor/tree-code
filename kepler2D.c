// Kepler 2D porblem

#include <stdio.h>
#include <complex.h>
#include <math.h>
#include <tgmath.h>
#include <omp.h>
#include <stdlib.h>

// maxima distancia sol tierra 152 098 232 km
// velocidad 29.3 km/s afelio
//perihelio 147 098 290 km velocidad 30.3 km/s
// Msun 1.98.529 *10^30 kg
// G =6.674*10**(-11) m**3 /(kg*s**2)

#define n 48 //number of integration steps
#define MSUN 1.98529E30
#define G 6.674E-11
#define x0 1.52098232E11
#define y0 0.0
#define vx0 0.0
#define vy0 2.93E4
#define full_orbits 5.1


#define NAME_OF_FILE1 "leapfrog.dat"
#define NAME_OF_FILE2 "rk4.dat"
int main(){
  
  FILE *fp1;
  FILE *fp2;
  double *x_leap ,*y_leap,*x_rk4,*y_rk4,*vx_leap ,*vy_leap,*vx_rk4,*vy_rk4;
  double prefactor=-G*MSUN;
  double dt=31557600.0*full_orbits/n;
  printf("%f \n",dt);
  printf("%f \n",prefactor);
  int i;
  
  fp1=fopen(NAME_OF_FILE1,"w");
  fp2=fopen(NAME_OF_FILE2,"w");

  
  x_leap = (double *) calloc(n, sizeof(double));
  vx_leap = (double *) calloc(n, sizeof(double));
  y_leap = (double *) calloc(n, sizeof(double));
  vy_leap = (double *) calloc(n, sizeof(double));
  x_rk4 = (double *) calloc(n, sizeof(double));
  vx_rk4 = (double *) calloc(n, sizeof(double));
  y_rk4 = (double *) calloc(n, sizeof(double));
  vy_rk4 = (double *) calloc(n, sizeof(double));
 


  
  *x_leap =x0;
  *vx_leap=vx0;
  *y_leap =y0;
  *vy_leap= vy0;
  *x_rk4 =x0; 
  *vx_rk4 =y0;
  *y_rk4 =vx0;
  *vy_rk4 =vy0;
  
    //  printf("%.1f%+.1fi cartesian is rho=%f theta=%f polar\n", creal(z), cimag(z), cabs(z), carg(z));
   

  double Force(double prefactor,double x, double y){
  return prefactor*x/pow(x*x+y*y,1.5);
}
  void rk4(double *x,double *y,double *vx,double *vy){
    double k1_x,k2_x,k3_x,k4_x,k1_y,k2_y,k3_y,k4_y,k1_vx,k2_vx,k3_vx,k4_vx,k1_vy,k2_vy,k3_vy,k4_vy;
    k1_x=*vx;
    k1_y=*vy;
    k1_vx=Force(prefactor,*x,*y); 
    k1_vy=Force(prefactor,*y,*x);    
    
    k2_x=*vx+k1_vx*dt*0.5;
    k2_y=*vy+k1_vy*dt*0.5;
    k2_vx=Force(prefactor,*x+k1_x*0.5*dt,*y+k1_y*0.5*dt); 
    k2_vy=Force(prefactor,*y+k1_y*0.5*dt,*x+k1_x*0.5*dt);
    
    k3_x=*vx+k2_vx*dt*0.5;
    k3_y=*vy+k2_vy*dt*0.5;
    k3_vx=Force(prefactor,*x+k2_x*0.5*dt,*y+k2_y*0.5*dt); 
    k3_vy=Force(prefactor,*y+k2_y*0.5*dt,*x+k2_x*0.5*dt); 

    k4_x=*vx+k3_vx*dt;
    k4_y=*vy+k3_vy*dt;
    k4_vx=Force(prefactor,*x+k3_x*dt,*y+k3_y*dt); 
    k4_vy=Force(prefactor,*y+k3_y*dt,*x+k3_x*dt); 
  
    
    *(x+1)=*x+dt*(1./6.)*(k1_x+2.*k2_x+2.*k3_x+k4_x);
    *(y+1)=*y+dt*(1./6.)*(k1_y+2.*k2_y+2.*k3_y+k4_y);
    *(vx+1)=*vx+dt*(1./6.)*(k1_vx+2.*k2_vx+2.*k3_vx+k4_vx);
    *(vy+1)=*vy+dt*(1./6.)*(k1_vy+2.*k2_vy+2.*k3_vy+k4_vy);
  } 
  void leap(double *x,double *y,double *vx,double *vy){
    //  printf("%f %f\n",*(x),*(y));
    *(x+1)=*x+dt**(vx);
    *(y+1)=*y+dt**(vy);
    *(vx+1)=*vx+dt*Force(prefactor,*(x+1),*(y+1));
    *(vy+1)=*vy+dt*Force(prefactor,*(y+1),*(x+1));   
  }      
  void print_to_a_file(FILE *fp,double *x,double *y){
  for(i=0;i<n;i++){
  //        printf("%f %f\n",*(c_real+i),*(c_img+j));
    fprintf(fp,"%f %f\n",*(x+i),*(y+i));
}}
  
  *(vx_leap)=*vx_leap+dt*0.5*Force(prefactor,*x_leap,*y_leap);  
  *(vy_leap)=*vy_leap+dt*0.5*Force(prefactor,*y_leap,*x_leap);
  
  //#pragma omp parallel for private(i)shared(x_leap,vx_leap,y_leap,vy_leap)//,x_rk4,vx_rk4,y_rk4,vy_rk4)
  
  for(i=0;i<n-1;i++){
    rk4((x_rk4+i),(y_rk4+i),(vx_rk4+i),(vy_rk4+i));
    leap((x_leap+i),(y_leap+i),(vx_leap+i),(vy_leap+i));  
  }
  
  *(vx_leap+i)=*vx_leap+dt*0.5*Force(prefactor,*(x_leap+i),*(y_leap+i));  
  *(vy_leap+i)=*vy_leap+dt*0.5*Force(prefactor,*(y_leap+i),*(x_leap+i));
  
    
  print_to_a_file(fp1,x_leap,y_leap);
  print_to_a_file(fp2,x_rk4,y_rk4);
  
  fclose(fp1);
  fclose(fp2);
  
  free(x_leap);
  free(vx_leap);
  free(y_leap);
  free(vy_leap);
  free(x_rk4);
  free(vx_rk4);
  free(y_rk4);
  free(vy_rk4);
  
  return(0);
}

