#include <stdio.h>
#include <complex.h>
#include <math.h>
#include <tgmath.h>
#include <omp.h>
#include <stdlib.h>
#include <string.h> //to use strcat
#include "utility.h"   // for using  MAX  needed for ran3()

#include<time.h>




#define THETA 0.5//// condition force
#define G 6.674E-11 /// gravitational cte//
#define COUNT 10 // some space for the tree print
#define TIME_STEPTS 1
#define NAME_OF_FILE "tree.dat"
struct tnode{
  double m;/// Mass of the node
  double x;// Mass center cordenates
  double y;
  double z;
  double x_max,y_max,z_max,x_min,y_min,z_min;
  struct tnode *one; //first child 3d eight childs
  struct tnode *two; //second child
  struct tnode *three; 
  struct tnode *four; 
  struct tnode *five; 
  struct tnode *six; 
  struct tnode *seven; 
  struct tnode *eight; 
};

struct particle{
  double m;
  double x;
  double y;
  double z;
  double vx;
  double vy;
  double vz;
};

struct force{
  double x;
  double y;
  double z;
};
struct graphs{
  double tpp;
  double ttree;
  double t_creation_tree;
  double error;
};

typedef struct graphs graphs_t ;
typedef struct particle particle_t;  // typedef (WHAT_YOU_WANT_TO_AVOID) (SHORTCUT)
typedef struct tnode tnode_t;
typedef struct force force_t ;

// functions headers

//-----------------------------------------------------------------------------------------------------------------------------------------\\\ \

tnode_t *create_a_tree(particle_t *,particle_t *,int);
tnode_t *add_particle(tnode_t *,particle_t *,particle_t *,double , double  ,double  ,double ,double ,double );
tnode_t *add_particle2(tnode_t *,particle_t *,particle_t *,double , double  ,double  ,double ,double ,double );
tnode_t *talloc(void);

force_t tree_force(particle_t *,tnode_t *,double,double);
force_t particle_particle_force(particle_t *,particle_t *,int,double);

int condition(particle_t *,tnode_t *,double);

double distance_square(particle_t *,tnode_t *);
double distance_square_particle_particle(particle_t *,particle_t *);
void print_tree(tnode_t *,int );
void free_tree(tnode_t *);
void print_to_a_file(FILE *,particle_t *,int,int);

graphs_t get_graphs(int ,double ,double );
