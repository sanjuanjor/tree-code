// second  mandelbrot implemetation using evrything i was tell to use

#include <stdio.h>
#include <complex.h>
#include <math.h>
#include <tgmath.h>
#include <omp.h>
#include <stdlib.h>
#include <time.h>


#define n 1000 //dimensions of the cell
#define minimg -1.1
#define maximg 1.1
#define minreal -2.
#define maxreal 1.1
#define nmaxinteration 1000
#define NAME_OF_FILE "mandelbrotfinal.txt"


int main(){


  omp_set_num_threads(4);
  int i,j,count;
  double complex z,c;
  double realstep=(maxreal-minreal)/n;
  double imgstep=(maximg-minimg)/n;
  FILE *fp;
  double *c_real ,*c_img;
  int *notdivergence;
  time_t t0,t1;
  double dt;
  fp=fopen(NAME_OF_FILE,"w");

  c_real = (double *) calloc(n, sizeof(double));
  c_img = (double *) calloc(n, sizeof(double));
  notdivergence = (int*) calloc(n*n,sizeof(int));


#pragma omp parallel for private(i) shared(c_real,c_img)
  for(i=0;i<n;i++){
    c_real[i]=minreal+realstep*i;
    c_img[i]=minimg+imgstep*i;
  }



   //  printf("%.1f%+.1fi cartesian is rho=%f theta=%f polar\n", creal(z), cimag(z), cabs(z), carg(z));
 

  int convergence(double complex c)
  {
    int count=0;
    double complex z=0.+0.*I;
      while(count<nmaxinteration && cabs(z)<2){
        count++;
        z=z*z+c;
      }
      if(count==nmaxinteration){
        //printf("%f %f\n",creal(c
        //),cimag(c));
        return(1);
        }
      else
        return(0);
  }
  void print_to_a_file(double *c_real,double *c_img,int *notdivergence)
  {
    //#pragma omp parallel for private(i,j) shared(c_real,c_img) (dont use this for io)    
  for(i=0;i<n;i++)
    for (j=0;j<n;j++)
      if(*(notdivergence+i+j*n)==1)
        //        printf("%f %f\n",*(c_real+i),*(c_img+j));
        fprintf(fp,"%f %f\n",*(c_real+i),*(c_img+j));
        
        }
  

  
  // there are two schedule static just divide for in the number of fors and dynamic when one finish give it another task
  t0=time(NULL);
#pragma omp parallel for private(i,j) shared(c_real,c_img,notdivergence) default(none) schedule(dynamic)
  for(i=0;i<n;i++){
    for (j=0;j<n;j++){
      //c=minreal+realstep*i+(minimg+imgstep*j)*I;
      notdivergence[i+n*j]=convergence(c_real[i]+c_img[j]*I);
    }
  }
  t1=time(NULL);
  dt=difftime(t1,t0);
  printf("%f \n",dt);
  print_to_a_file(c_real,c_img,notdivergence);

  fclose(fp);
  free(c_real);
  free(c_img);
  free(notdivergence);


  return(0);
}
