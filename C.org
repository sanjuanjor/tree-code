* Supercomputer 
** Lecture 1
****   Mechanical Machines
     Abacus BC
     Blaise Pascal mechanical calculator 1642
     Charles Babbage "difference engine" 1834
   
***   First electrical Herman Hollerith Punch cards adder to do a census
    In the USA computing tabulating recording corportation
    International Business Machines (IBM)
   
****   First general propuse machines
***** Turing Machine (just theorical)
     bassed upon in sequential memory (isntead of random)
     Turing test
     If a human interact with a computer if you cant tell you are
     talking to a human or not the compuetr is said to think
***** Konrad Zuse 
     First bluild machine
     Z1: all the calculations where done in binary
     freely programmable via puch cards (you enter two numbers and then the operation)
     floating point operations
     i/o in decimal system thought
     Z3:
     Arquitecture symilar to today 
***** Colossus
      To desencrypt German messages
      See Colosus movies
*** Supercomputers Now
    flop/s(floating operations per second)
    #2 Tianhe-2(China) 
      50*10^15 flop/s
      Linux
      3.120.000 cores ,500TB RAM
    #16 Marenostrum 
      10 Pflop/s
*** Simulation of galxies
    Eirk Holmberg  Lamps
    [[https://www.cosmosim.org/][Database of simulations]]
    Simulation of galaxies formation in small scaless doesn't
    correspond with the observations we expected more drawf galaxies 
     
    Physical phenomenon \rightarrow  Mathematical model
    \begin{itemize}
    \item Domain discretisation: grid introduction , particle sampling.
    \item Numerical algorithm
    \item Coding
    \item Simulation
    \item Analysis      
    \end{itemize}

*** Domain discretisation  
    | L P.Euler | vs                | J. L Lagrange |
    |-----------+-------------------+---------------|
    | Gird      | vs                | Particle      |
    |           | Mass conservation |               |
    |           | Mo                |               |



** Lecture 2
   serial machine = CPU- Cache- RAM
***   CPU very primitive commands.(Instructions asamblers)
    cycle chain:
      fetch get instructions and/or data from memory
      decode store instruction and/or data in register
      execute perform instruction
    
    Arithmetical/logical instructions +/ - , bitshift(*,/), if.
    
    Some CPU allow multi-threding 
    ie alredy fetch next intruction while still executing
    
    excution time =nummber of instrucitons * CPI(cycle per
    instruction)*t_c(time per cycle)
**** speed up your code 
     Improve your algorithm to require less instructions(eg.
     calcule factors outside loops)
     Use more adequate instructions (eg pow(x,2) its worst than x*x )
*** RAM 
**** storage in bynari 
    1 bit = 0 or 1
    8 bits = 1 byte
    4 bytes = 1 float(= 32 bits)
    8 bytes = 1 double(= 64 bits)
**** latency time for memory acces (nano-secnods)
**** speed-ups 
     Multi-therading CPUS
     cleaver usage of cache     
*** Cache      
   when fetch a[i] also fetch a[i+1] into Cache 
   nowadays multiple Cache level
   bad programing will lead to Cache misses 
   time=cache hit rate * cache access time +(1- cahce hit rate)*ram
   acces time
   example time cache 10 ns  ram acces time 100ns
   If cache hit rate is only 0.1 execution time will be 91 ns
   If cache hit rate is 0.9 excution time will be 19 ns
****   Caches hits and misses 
     In C 2D array in C density[2][3]
     memory alignment depends on the lenguage row mayor ordering in C
     Your faster lop should be in the outer most index density[0][0]
     \rightarrow density[0][1] density[0][2] 

For perfromance the frecuency of the bus (cache link to the CPU) its
more important than the CPU frequency.And the with of the bus.
128 bit integers (how to use them)
*** Out of core applications
    Applications thath use hard-drive 
    Practically all database work like this
*** Multicored  any other posibily to spped things up?
**** shared memory architecture 
     They share RAM easy to adapt existing serial code 
     Limited by RAM to be placed into a single machine
     OpenMP
     most commonly used standar to paralllalize code on shared memory
     architectures
     primarly distribute for llop components onto different CPUs
     the latency betweenn CPU is negible
     the only problem its both acces to the same data
**** distributed memory architecture
     Various computer 
     perforance higly sensiteve to interconnect
     MPI message Passing Interface open-mpi.org 
     there is no diference  in the source code the only difference its the varible who am i
     one number for each computer
**** Real machines 
     BLue gene/L
   [[https://www.top500.org/lists/][top 500]]
Grid computing Distributing the work between diferent computers
Cloud computing use remote resources for your caclculations
GRID computing 
SETI HOME  they look for something near to the 21 cm line in radio
   telescope so they developed a screensaver thath analised the data
   if you download it.
the bigest now bitcoin network 

**** your algorthm must be parallel

one number is cx the next number is a funtion of x the next its a
function of the funton of x and so on
serial algorithm 
#+BEGIN_SRC C
a[0]= STARTVALUE;
for(i=1; i<N ; i++){
  a[i]= function(a[i-1]);
}

#+END_SRC
parallel algorithm
shared memory architecture

#+BEGIN_SRC C
a[0]=startvalue;
for(i=1;i<N;i++){
  b=[0]
  for(j=0;j<i;j++){
    b=function(b);
}
a[i]=b;
}
#+END_SRC

Now the i loop can be paraliced
#+BEGIN_SRC C
#pragma omp parallel for private(i,j,b) shared(a)
#+END_SRC

You just need to add this befroe the loop that you want to parallelize

when you want to  parallelize you need to look up a your varibles and
decided which of them are private and which of them are shared

| CPU1         | CPU2     | CPU3         |
| a[0]a[1]a[2] | a[3]a[4] | a[5]a[5]a[7] |
Strong scaling it scales fixed size but incrised the number of CPU you
aim to runging a given problme as fast as possilbe 
Weak scaling keep the number of the COU fixed but you incresed the
problem size you aim runing the largest posible problem in a given
amount of time



** How to actually wirte a program

*** define the problem
*** decide on organisation
****    choose essential elements(variables,structures,etc)
****    shape relevant tasks
    devide data into sub-sets but perform the same task in each subset
    
****    desing your algorithm to be parallelizable
    Data parallelization
    Task parallelization
****    draw a flowchart
*** code in your preferred language
*** test code using simple/knwon test cases
*** sumary of coding recommnedations
    make prper use of cache
    avoid conditons,I/0 and sub rotuines calls inside loops
#+BEGIN_SRC C
for (i)
  if(i<N/2)
  
  else
#+END_SRC

#+BEGIN_SRC C 
for(i<N/2)
for(i>=N/2)
#+END_SRC
avoid unnecessary operations inside loops in general
use multiplications rather than divisions or powers
#define pow2(x) ((x)*(x))
keep it modular
keep it simple
* Review of numerical methods
** Lecture 3
*** Root finding
    example know m M /Omega_/Lambda /Omega_m
    unknown z
    o=f(z)=m
    bi-section method
    z_0=(a+b)/2
    while(abs(b-a)>eps){
    z0= \sum{\inf}
**** Root of functions
**** Linear algebragic equations
**** Optimisation
     
*** Curve fitting
*** Integration
*** Solving diferrential equations
**** Ordinary differential equations
     0=g(f(n),f(n-1),....f^{(1)},f)
     we asummed explicit differential equations if you can separete the
     derivative in one side.
*****     First order
     \frac{df}{dt}=G(f,t)

     \frac{\Delta f}{\Delta t} =
     \frac{ f(t_{i+1} - f(t_i) }{ t_{i+1}-t_i } =\frac{f_{i+1}-f{i}}{t_{i+1}-t_i}=G(f_i,t_i)
     f_{i+1}= f_i + \Delta t G(f_i , t_i)
     
****** Euler scheme
     f_{i+1}= f_i + \Delta t G(f_i , t_i)
     Local error estimate 
     Global error estimate
     First order acurate
******  Modified Euler scheme
      Trial step and take the avarage slope
      f_{i+1}=f_{i}+\Delta t \frac{G_i+G^*_{i+1}}{2}

      G*_i=G_{i+1}
      Second order accurate
      
****** 4 order Runge-Kutta scheme
***** Second order equations can decomposed to a system in a system of couple first order equations
***** Leap-frog scheme (fastest seconde order scheme
\frac{df}{dt}
f_{i+1}=f_i+\Delta t h_{i+1/2}
I need to jump start one of the functions
I need to reajust it at the end.
sympletic squeme implies energy conservation
sympletic implies it conserved the geometry strcutrue of the original
hamiltonian flow.


**** Partial differential equations



 
   



*** Actual set of equations   
*** Taylor series
    f(x)=f(x_0)+\sum^\infnty_{n=1} \frac{f^{(n)}(x_0)}{n!}(x-x_0)^n
*** debugging your code 

    time integration
    x_n=x+e_n (true solutinon + error
    Mth order accurate integration hceme
    to check if my code its doing what its suppose to do 
    i use 3 different number of integration stepts
    and i see if the the error scale as it should
    e_n-e_m=C
    em-el=C \delta t_m^M - \delta t_l^M
*** How do you pick your integration step

    \delta t must be smaller thant any dinamical time of the system 
    \delta t acceleration velcoty criterion
    \delta t <= \sqrt{\frac{\epsilon}{a_max}}
    \delta t <= \frac{\epsilon}{v_max}
    v_maximun speed thath you will have in your problem information
    cant travel faster 
*** momentun consrvation

    \abs{\sum F}=0 
    practical test
    \frac{\abs{\sum F}=0 }{\sum \abs[F}} = 10^-4




    
* Summary of relevant astrophysical processes 
* Gravity solvers
* Hydrodynamimcs solvers
* Evaluation
** exercises 5/10
***   Mandelbrot 
***   Kepler 
***   1D SPH code
** project 5/10
* Hands-on exercises 
#+BEGIN_SRC C :results output

#include <stdio.h> 
int main(){
   // printf() displays the string inside quotation
   printf("Hello, World!");
int i;
int N =5;
#pragma omp parallel for shared(i)
for(i=1;i<N;i++){
printf("%d\n", i);
}   
return 0;


}
#+END_SRC

#+RESULTS:
: Hello, World!1
: 2
: 3
: 4
