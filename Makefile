COMPILER	= gcc
FLAGS 		= -O2  -fopenmp -g 
LIBRARIES 	= -lm
OBJECTS         = utility.o	#treecode_functions.o	
#the math libraries
#name of the rule and wht it depends on


test00: HelloWorld.c #other dependencies second line need a tab and its the comand
	$(COMPILER) $(FLAGS) -o HelloWorld HelloWorld.c $(LIBRARIES)


test01: stdint.c
	$(COMPILER) $(FLAGS) -o stdint stdint.c $(LIBRARIES)
test02: foverflow.c
	$(COMPILER) $(FLAGS) -o foverflow foverflow.c $(LIBRARIES)
test03: mandelbrot.c
	$(COMPILER) $(FLAGS) -o mandelbrot mandelbrot.c $(LIBRARIES)
test04: mandelbrot2.c
	$(COMPILER) $(FLAGS) -o mandelbrot2 mandelbrot2.c $(LIBRARIES)
test05: kepler2D.c
	$(COMPILER) $(FLAGS) -o kepler2D kepler2D.c $(LIBRARIES)
test06: treecode.c $(OBJECTS)
	$(COMPILER) $(FLAGS) -o treecode treecode.c treecode_functions.c $(OBJECTS) $(LIBRARIES)
#treecdoe_functions.o: treecode_functions.c
#	$(COMPILER) $(FLAGS) -c treecode_functions.c $(LIBS)
test07: sph.c
	$(COMPILER) $(FLAGS) -o sph sph.c $(OBJECTS) $(LIBRARIES)
utility.o:	utility.c utility.h
	$(COMPILER) $(FLAGS) -c utility.c $(LIBRARIES)

clean:
	rm -rf *.o *.dSYM  HelloWorld stdint foverflow Pointer Array1D Array3D ParallelRecursion1D ParallelRecursion1Dimproved Structure StructureArray FunctionPointer StructurePointer indexx qsort IO valgrind read_mtree mandelbrot mandelbrot2 mandelbrotfinal.txt treecode sph treecode_functions.o 
