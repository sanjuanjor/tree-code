#include "define.h"




tnode_t *create_a_tree(particle_t *p,particle_t *p2,int N_particles){
  tnode_t *root=NULL;
  int i;
  //calcular xmax etc
  double x_min,y_min,z_min;
  double x_max,y_max,z_max;
  double lenght;
  
  i=0;
  x_max=(p+i)->x;
  x_min=(p+i)->x;
  y_max=(p+i)->y;
  y_min=(p+i)->y;
  z_max=(p+i)->z;
  z_min=(p+i)->z;
  for(i=0;i<N_particles;i++){
    
    if((p+i)->x>x_max)
      x_max=(p+i)->x;
    if((p+i)->x<x_min)
      x_min=(p+i)->x;
    if((p+i)->y>y_max)
      y_max=(p+i)->y;
    if((p+i)->y<y_min)
      y_min=(p+i)->y;
    if((p+i)->z>z_max)
      z_max=(p+i)->z;
    if((p+i)->z<z_min)
      z_min=(p+i)->z;
  }

  // we should make it to be of the same lenght in all dimension so the force criterion have any sense 
  double x_center,y_center,z_center;
  x_center=(x_max-x_min)/2.+x_min;
  y_center=(y_max-y_min)/2.+y_min;
  z_center=(z_max-z_min)/2.+z_min;

  lenght=MAX(MAX(x_max-x_min,y_max-y_min),z_max-z_min);

  x_max=x_center+lenght/2.;
  y_max=y_center+lenght/2.;
  z_max=z_center+lenght/2.;

  x_min=x_center-lenght/2.;
  y_min=y_center-lenght/2.;
  z_min=z_center-lenght/2.;

  // now we have a cube   
  
  for(i=0;i<N_particles;i++){
    //    fprintf(stderr,"%f %f %f \n",(p+i)->x,(p+i)->y,(p+i)->z);
    root=add_particle(root,(p+i),p2, x_max , x_min , y_max , y_min , z_max , z_min); 
  }
  
}

tnode_t *add_particle(tnode_t *node,particle_t *p,particle_t *p2,double x_max, double x_min ,double y_max ,double y_min,double z_max,double z_min)
{
  //fprintf(stderr,"%f %f %f %f %f %f \n",x_max,x_min,y_max,y_min,z_max,z_min);
  double M;



  if(node==NULL){
    //tnode_t *node;// make a new node

    node=talloc();
    node->x_max=x_max , node->y_max=y_max , node->z_max=z_max , node->x_min=x_min ,node->y_min=y_min , node->z_min=z_min;
    node->x=p->x;
    node->y=p->y;
    node->z=p->z;
    node->m=p->m;
    node->one=node->two=node->three=node->four=node->five=node->six=node->seven=node->eight=NULL;
    //  fprintf(stderr,"%f\n",M); 
    //printf("%f\n",node->m);
    
  }
  else if(node->one!=NULL || node->two!=NULL ||  node->three!=NULL || node->four!=NULL || node->five!=NULL || node->six!=NULL || node->seven!=NULL || node->eight!=NULL){
    //update mass and center of mass
    M=node->m+p->m;
    node->x = (p->x*p->m + node->x*node->m) / M;
    node->y = (p->y*p->m + node->y*node->m) / M;
    node->z = (p->z*p->m + node->z*node->m) / M;
    node->m =M;

    //fprintf(stderr,"%f\n",M);

    double x_center,y_center,z_center;
    x_center=(x_max-x_min)/2.+x_min;
    y_center=(y_max-y_min)/2.+y_min;
    z_center=(z_max-z_min)/2.+z_min;

    if (p->x <= x_center  && p->y <= y_center  && p->z <= z_center) 
      /* fprintf(stderr,"1p\n"), */  node->one=add_particle(node->one, p , p2 ,x_center ,x_min , y_center, y_min , z_center , z_min );
    else if (p->x <= x_center && p->y <= y_center  && p->z > z_center) 
      /* fprintf(stderr,"2p\n"), */  node->two=add_particle(node->two, p , p2 ,x_center ,x_min , y_center, y_min ,z_max, z_center );
    else if (p->x <= x_center && p->y > y_center &&  p->z <= z_center) 
      /* fprintf(stderr,"3p\n"), */  node->three=add_particle(node->three , p , p2 ,x_center ,x_min , y_max, y_center , z_center,z_min );
    else if (p->x <= x_center &&  p->y > y_center  && p->z > z_center) 
      /* fprintf(stderr,"4p\n"),  */ node->four=add_particle(node->four , p , p2 , x_center,x_min , y_max, y_center , z_max, z_center);
    else if (p->x > x_center && p->y <= y_center  && p->z <= z_center) 
      /* fprintf(stderr,"5p\n"), */  node->five=add_particle(node->five, p , p2 , x_max , x_center , y_center, y_min , z_center ,z_min );
    else if (p->x > x_center && p->y <= y_center  && p->z > z_center ) 
      /* fprintf(stderr,"6p\n"), */  node->six=add_particle(node->six, p , p2 ,  x_max , x_center , y_center, y_min , z_max, z_center ) ;
    else if (p->x > x_center && p->y > y_center && p->z <= z_center ) 
      /* fprintf(stderr,"7p\n"), */  node->seven=add_particle(node->seven, p , p2 , x_max , x_center , y_max, y_center , z_center,z_min );
    else if (p->x > x_center &&  p->y > y_center && p->z > z_center) 
      /* fprintf(stderr,"8p\n"), */  node->eight=add_particle(node->eight , p , p2 , x_max , x_center , y_max , y_center , z_max, z_center );

    
  }    


    else if(node->one==NULL && node->two==NULL &&  node->three==NULL && node->four==NULL && node->five==NULL && node->six==NULL && node->seven==NULL && node->eight == NULL) { //is a lea    {
      
    
    p2->x=node->x;
    p2->y=node->y;
    p2->z=node->z;
    p2->m=node->m;
    
    
    M=node->m+p->m;
    node->x = ( p->x * p->m + node->x * node->m) / M;
    node->y = ( p->y * p->m + node->y * node->m) / M;
    node->z = ( p->z * p->m + node->z * node->m) / M;
    node->m = M ;
    double x_center,y_center,z_center;
    x_center=(x_max-x_min)/2.+x_min;
    y_center=(y_max-y_min)/2.+y_min;
    z_center=(z_max-z_min)/2.+z_min;
  
    
    
    if (p->x <= x_center && p2->x <= x_center && p->y <= y_center && p2->y <= y_center && p->z <= z_center && p2->z <= z_center) 
    /* fprintf(stderr,"1\n"), */  node->one=add_particle2(node->one, p , p2 ,x_center ,x_min , y_center, y_min , z_center , z_min );
  else if (p->x <= x_center && p2->x <= x_center && p->y <= y_center && p2->y <= y_center && p->z > z_center && p2->z > z_center) 
    /* fprintf(stderr,"2\n"), */  node->two=add_particle2(node->two, p , p2 ,x_center ,x_min , y_center, y_min ,z_max, z_center );
  else if (p->x <= x_center && p2->x <= x_center && p->y > y_center && p2->y > y_center && p->z <= z_center && p2->z <= z_center) 
    /* fprintf(stderr,"3\n"), */  node->three=add_particle2(node->three , p , p2 ,x_center ,x_min , y_max, y_center , z_center,z_min );
  else if (p->x <= x_center && p2->x <= x_center && p->y > y_center && p2->y > y_center && p->z > z_center && p2->z > z_center) 
    /* fprintf(stderr,"4\n"), */  node->four=add_particle2(node->four , p , p2 , x_center,x_min , y_max, y_center , z_max, z_center);
  else if (p->x > x_center && p2->x > x_center && p->y <= y_center && p2->y <= y_center && p->z <= z_center && p2->z <= z_center) 
    /* fprintf(stderr,"5\n") ,  */ node->five=add_particle2(node->five, p , p2 , x_max , x_center , y_center, y_min , z_center ,z_min );
  else if (p->x > x_center && p2->x > x_center && p->y  <= y_center && p2->y <= y_center && p->z > z_center && p2->z > z_center) 
    /* fprintf(stderr,"6\n"), */  node->six=add_particle2(node->six, p , p2 ,  x_max , x_center , y_center, y_min , z_max, z_center ) ;
  else if (p->x > x_center && p2->x > x_center && p->y > y_center && p2->y > y_center && p->z <= z_center && p2->z <= z_center) 
    /* fprintf(stderr,"7\n"), */  node->seven=add_particle2(node->seven, p , p2 , x_max , x_center , y_max, y_center , z_center,z_min );
  else if (p->x > x_center && p2->x > x_center && p->y > y_center && p2->y > y_center && p->z > z_center && p2->z > z_center) 
    /* fprintf(stderr,"8\n"), */  node->eight=add_particle2(node->eight , p , p2 , x_max , x_center , y_max , y_center , z_max, z_center );
    
    else
      {
        //put the particle p in the correspondent node;
        if (p->x <= x_center  && p->y <= y_center  && p->z <= z_center) 
          /* fprintf(stderr,"1p\n"), */ node->one=add_particle(node->one, p , p2 ,x_center ,x_min , y_center, y_min , z_center , z_min );
        else if (p->x <= x_center && p->y <= y_center  && p->z > z_center) 
          /* fprintf(stderr,"2p\n"), */  node->two=add_particle(node->two, p , p2 ,x_center ,x_min , y_center, y_min ,z_max, z_center );
        else if (p->x <= x_center && p->y > y_center &&  p->z <= z_center) 
          /* fprintf(stderr,"3p\n"), */  node->three=add_particle(node->three , p , p2 ,x_center ,x_min , y_max, y_center , z_center,z_min );
        else if (p->x <= x_center &&  p->y > y_center  && p->z > z_center) 
          /* fprintf(stderr,"4p\n"), */  node->four=add_particle(node->four , p , p2 , x_center,x_min , y_max, y_center , z_max, z_center);
        else if (p->x > x_center && p->y  <= y_center  && p->z <= z_center) 
          /* fprintf(stderr,"5p\n"), */  node->five=add_particle(node->five, p , p2 , x_max , x_center , y_center, y_min , z_center ,z_min );
        else if (p->x > x_center && p->y  <= y_center  && p->z > z_center ) 
          /* fprintf(stderr,"6p\n"), */  node->six=add_particle(node->six, p , p2 ,  x_max , x_center , y_center, y_min , z_max, z_center ) ;
        else if (p->x > x_center && p->y > y_center && p->z <= z_center ) 
          /* fprintf(stderr,"7p\n"), */  node->seven=add_particle(node->seven, p , p2 , x_max , x_center , y_max, y_center , z_center,z_min );
        else if (p->x > x_center &&  p->y > y_center && p->z > z_center) 
          /* fprintf(stderr,"8p\n"), */  node->eight=add_particle(node->eight , p , p2 , x_max , x_center , y_max , y_center , z_max, z_center );


        /* //        put the particle p2 in the correspondent node; */

        if (p2->x <= x_center  && p2->y <= y_center  && p2->z <= z_center) 
          /* fprintf(stderr,"1p2\n"),   */node->one=add_particle(node->one, p2 , p ,x_center ,x_min , y_center, y_min , z_center , z_min );
        else if (p2->x <= x_center && p2->y <= y_center  && p2->z > z_center) 
          /* fprintf(stderr,"2p2\n"),   */node->two=add_particle(node->two, p2 , p ,x_center ,x_min , y_center, y_min ,z_max, z_center );
        else if (p2->x <= x_center && p2->y > y_center &&  p2->z <= z_center) 
          /* fprintf(stderr,"3p2\n"),  */ node->three=add_particle(node->three , p2 , p ,x_center ,x_min , y_max, y_center , z_center,z_min );
        else if (p2->x <= x_center &&  p2->y > y_center  && p2->z > z_center) 
          /* fprintf(stderr,"4p2\n"), */  node->four=add_particle(node->four , p2 , p , x_center,x_min , y_max, y_center , z_max, z_center);
        else if (p2->x > x_center && p2->y  <= y_center  && p2->z <= z_center) 
          /* fprintf(stderr,"5p2\n"), */  node->five=add_particle(node->five, p2 ,p , x_max , x_center , y_center, y_min , z_center ,z_min );
        else if (p2->x > x_center && p2->y  <= y_center  && p2->z > z_center ) 
          /* fprintf(stderr,"6p2\n"), */  node->six=add_particle(node->six, p2 , p ,  x_max , x_center , y_center, y_min , z_max, z_center ) ;
        else if (p2->x > x_center && p2->y > y_center && p2->z <= z_center ) 
          /* fprintf(stderr,"7p2\n"),   */node->seven=add_particle(node->seven, p2 , p , x_max , x_center , y_max, y_center , z_center,z_min );
        else if (p2->x > x_center &&  p2->y > y_center && p2->z > z_center) 
          /* fprintf(stderr,"8p2\n"), */  node->eight=add_particle(node->eight , p2 , p , x_max , x_center , y_max , y_center , z_max, z_center );
 
        

 

      
            
      }
      }

    
  
  return node;
}



tnode_t *add_particle2(tnode_t *node,particle_t *p,particle_t *p2,double x_max, double x_min ,double y_max ,double y_min,double z_max,double z_min)
{
  double M;
  //tnode_t *node;
  node = talloc();
  node->one = node->two = node->three = node->four = node->five = node->six = node->seven = node->eight = NULL;
  node->x_max=x_max , node->y_max=y_max , node->z_max=z_max , node->x_min=x_min ,node->y_min=y_min , node->z_min=z_min;
  M=p2->m+p->m;
  node->x = (p->x*p->m + p2->x*p2->m) / M;
  node->y = (p->y*p->m + p2->y*p2->m) / M;
  node->z = (p->z*p->m + p2->z*p2->m) / M;
  node->m =M;
  //fprintf(stderr,"2_%f\n",M);
  //fprintf(stderr,"%f %f %f %f %f %f\n",p2->x,p->x,p2->y,p->y,p2->z,p->z);
  // fprintf(stderr,"%f %f %f %f %f %f\n",x_max,x_min,y_max,y_min,z_max,z_min);
  double x_center,y_center,z_center;
  x_center=(x_max-x_min)/2.+x_min;
  y_center=(y_max-y_min)/2.+y_min;
  z_center=(z_max-z_min)/2.+z_min;
  
  if (p->x <= x_center && p2->x <= x_center && p->y <= y_center && p2->y <= y_center && p->z <= z_center && p2->z <= z_center) 
    /* fprintf(stderr,"1\n"),   */node->one=add_particle2(node->one, p , p2 ,x_center ,x_min , y_center, y_min , z_center , z_min );
  else if (p->x <= x_center && p2->x <= x_center && p->y <= y_center && p2->y <= y_center && p->z > z_center && p2->z > z_center) 
    /* fprintf(stderr,"2\n"),   */node->two=add_particle2(node->two, p , p2 ,x_center ,x_min , y_center, y_min ,z_max, z_center );
  else if (p->x <= x_center && p2->x <= x_center && p->y > y_center && p2->y > y_center && p->z <= z_center && p2->z <= z_center) 
    /* fprintf(stderr,"3\n"),   */node->three=add_particle2(node->three , p , p2 ,x_center ,x_min , y_max, y_center , z_center,z_min );
  else if (p->x <= x_center && p2->x <= x_center && p->y > y_center && p2->y > y_center && p->z > z_center && p2->z > z_center) 
    /* fprintf(stderr,"4\n"),   */node->four=add_particle2(node->four , p , p2 , x_center,x_min , y_max, y_center , z_max, z_center);
  else if (p->x > x_center && p2->x > x_center && p->y <= y_center && p2->y <= y_center && p->z <= z_center && p2->z <= z_center) 
    /* fprintf(stderr,"5\n"),   */node->five=add_particle2(node->five, p , p2 , x_max , x_center , y_center, y_min , z_center ,z_min );
  else if (p->x > x_center && p2->x > x_center && p->y <= y_center && p2->y <= y_center && p->z > z_center && p2->z > z_center) 
    /* fprintf(stderr,"6\n"),   */node->six=add_particle2(node->six, p , p2 ,  x_max , x_center , y_center, y_min , z_max, z_center ) ;
  else if (p->x > x_center && p2->x > x_center && p->y > y_center && p2->y > y_center && p->z <= z_center && p2->z <= z_center) 
    /* fprintf(stderr,"7\n"),   */node->seven=add_particle2(node->seven, p , p2 , x_max , x_center , y_max, y_center , z_center,z_min );
  else if (p->x > x_center && p2->x > x_center && p->y > y_center && p2->y > y_center && p->z > z_center && p2->z > z_center) 
    /* fprintf(stderr,"8\n"),   */node->eight=add_particle2(node->eight , p , p2 , x_max , x_center , y_max , y_center , z_max, z_center );
      else
      {
        //put the particle p in the correspondent node;
        if (p->x <= x_center  && p->y <= y_center  && p->z <= z_center) 
          /* fprintf(stderr,"1p\n"),   */node->one=add_particle(node->one, p , p2 ,x_center ,x_min , y_center, y_min , z_center , z_min );
        else if (p->x <= x_center && p->y <= y_center  && p->z > z_center) 
          /* fprintf(stderr,"2p\n"),   */node->two=add_particle(node->two, p , p2 ,x_center ,x_min , y_center, y_min ,z_max, z_center );
        else if (p->x <= x_center && p->y > y_center &&  p->z <= z_center) 
          /* fprintf(stderr,"3p\n"),  */ node->three=add_particle(node->three , p , p2 ,x_center ,x_min , y_max, y_center , z_center,z_min );
        else if (p->x <= x_center &&  p->y  > y_center  && p->z > z_center) 
          /* fprintf(stderr,"4p\n"),   */node->four=add_particle(node->four , p , p2 , x_center,x_min , y_max, y_center , z_max, z_center);
        else if (p->x > x_center && p->y  <= y_center  && p->z <= z_center) 
          /* fprintf(stderr,"5p\n"),   */node->five=add_particle(node->five, p , p2 , x_max , x_center , y_center, y_min , z_center ,z_min );
        else if (p->x > x_center && p->y  <= y_center  && p->z > z_center ) 
          /* fprintf(stderr,"6p\n"),  */ node->six=add_particle(node->six, p , p2 ,  x_max , x_center , y_center, y_min , z_max, z_center ) ;
        else if (p->x > x_center && p->y > y_center && p->z <= z_center ) 
          /* fprintf(stderr,"7p\n"),   */node->seven=add_particle(node->seven, p , p2 , x_max , x_center , y_max, y_center , z_center,z_min );
        else if (p->x > x_center &&  p->y > y_center && p->z > z_center) 
          /* fprintf(stderr,"8p\n"),   */node->eight=add_particle(node->eight , p , p2 , x_max , x_center , y_max , y_center , z_max, z_center );


        /* //        put the particle p2 in the correspondent node; */

        if (p2->x <= x_center  && p2->y <= y_center  && p2->z <= z_center) 
          /* fprintf(stderr,"1p2\n"),   */node->one=add_particle(node->one, p2 , p ,x_center ,x_min , y_center, y_min , z_center , z_min );
        else if (p2->x <= x_center && p2->y <= y_center  && p2->z > z_center) 
          /* fprintf(stderr,"2p2\n"), */  node->two=add_particle(node->two, p2 , p ,x_center ,x_min , y_center, y_min ,z_max, z_center );
        else if (p2->x <= x_center && p2->y > y_center &&  p2->z <= z_center) 
          /* fprintf(stderr,"3p2\n"), */  node->three=add_particle(node->three , p2 , p ,x_center ,x_min , y_max, y_center , z_center,z_min );
        else if (p2->x <= x_center &&  p2->y > y_center  && p2->z > z_center) 
          /* fprintf(stderr,"4p2\n"), */  node->four=add_particle(node->four , p2 , p , x_center,x_min , y_max, y_center , z_max, z_center);
        else if (p2->x > x_center && p2->y  <= y_center  && p2->z <= z_center) 
          /* fprintf(stderr,"5p2\n"), */  node->five=add_particle(node->five, p2 ,p , x_max , x_center , y_center, y_min , z_center ,z_min );
        else if (p2->x > x_center && p2->y  <= y_center  && p2->z > z_center ) 
          /* fprintf(stderr,"6p2\n"), */  node->six=add_particle(node->six, p2 , p ,  x_max , x_center , y_center, y_min , z_max, z_center ) ;
        else if (p2->x > x_center && p2->y > y_center && p2->z <= z_center ) 
          /* fprintf(stderr,"7p2\n"), */  node->seven=add_particle(node->seven, p2 , p , x_max , x_center , y_max, y_center , z_center,z_min );
        else if (p2->x > x_center &&  p2->y > y_center && p2->z > z_center) 
          /* fprintf(stderr,"8p2\n"),  */ node->eight=add_particle(node->eight , p2 , p , x_max , x_center , y_max , y_center , z_max, z_center );

      }
      
  
  return node;
}




tnode_t *talloc(void) 
{ 
  return (tnode_t *) malloc(sizeof(tnode_t)); 
} 


void print_tree(tnode_t *node, int space)
{
  if(node !=NULL){
    // printf(strcat(prefix,"\n"));
    
    //if(node->one!=NULL)

    space+= COUNT;
    print_tree(node->one,space);//,strcat(prefix,"|  "));
    // if(node->two!=NULL)
    print_tree(node->two,space);//,strcat(prefix,"|  "));
    // if(node->three!=NULL)
    print_tree(node->three,space);//,strcat(prefix,"|  "));
    //    if(node->four!=NULL)
    print_tree(node->four,space);//,strcat(prefix,"|  "));

    fprintf(stdout,"\n");
    for(int j = COUNT; j<space;j++)
      fprintf(stdout," ");
    fprintf(stdout,"%f\n",node->m);

    //if(node->five!=NULL)
    print_tree(node->five,space );//,strcat(prefix,"|  "));
      //if(node->six!=NULL)
    print_tree(node->six,space );//,strcat(prefix,"|  "));
      //if(node->seven!=NULL)
    print_tree(node->seven,space );//,strcat(prefix,"|  "));
    //if(node->eight!=NULL)
    print_tree(node->eight,space );//,strcat(prefix,"|  "));
  }
}

void free_tree(tnode_t *node)
{
  if(node != NULL)
    {
      free_tree(node->one);
      free_tree(node->two);
      free_tree(node->three);
      free_tree(node->four);
      free_tree(node->five);
      free_tree(node->six);
      free_tree(node->seven);
      free_tree(node->eight);

      // if there was any arrays in a node i should free them too
      free(node);
    }
  
}


force_t particle_particle_force(particle_t *p1,particle_t *p2,int N_particles,double epsilon){
  force_t F;
  double prefactor;
  int i;
  F.x=0,F.y=0,F.z=0.;
  for(i=0;i<N_particles;i++){
    if(p1!=(p2+i)){
      prefactor=p2->m*G/pow(distance_square_particle_particle(p1,(p2+i))+epsilon*epsilon,1.5);
      F.x+=prefactor*((p2+i)->x-p1->x);
      F.y+=prefactor*((p2+i)->y-p1->y);
      F.z+=prefactor*((p2+i)->z-p1->z);
  }
  }
  return F;
}


force_t tree_force(particle_t *p,tnode_t *node,double theta,double epsilon)
{
  force_t F;
  double prefactor;
  
  if (node->one == NULL && node->two == NULL && node->three ==NULL && node->four==NULL && node->five==NULL && node->six==NULL
      && node->seven==NULL && node->eight==NULL) //leaf and its not itself
    
    {
      if((node->x!=p->x || node->y!=p->y || node->z!=p->z ) ){
        prefactor=node->m*G/pow(distance_square(p,node)+epsilon*epsilon,1.5);
        F.x=prefactor*(node->x-p->x);
        F.y=prefactor*(node->y-p->y);
        F.z=prefactor*(node->z-p->z);
        return F;
      }
      else{
        F.x=0.,F.y=0.,F.z=0.;
        return F;
      }
        }
  else if(condition(p,node,theta)){ 
    prefactor=node->m*G/pow(distance_square(p,node)+epsilon*epsilon,1.5);
    F.x=prefactor*(node->x-p->x);
    F.y=prefactor*(node->y-p->y);
    F.z=prefactor*(node->z-p->z);
    //    fprintf(stderr,"1\n");
    return F;
  }
  else{
    force_t F1,F2,F3,F4,F5,F6,F7,F8;
    F1.x=0.,F2.x=0.,F3.x=0.,F4.x=0.,F5.x=0.,F6.x=0.,F7.x=0.,F8.x=0.;
    F1.y=0.,F2.y=0.,F3.y=0.,F4.y=0.,F5.y=0.,F6.y=0.,F7.y=0.,F8.y=0.;
    F1.z=0.,F2.z=0.,F3.z=0.,F4.z=0.,F5.z=0.,F6.z=0.,F7.z=0.,F8.z=0.;
    
    if (node->one !=NULL)
      F1=tree_force(p,node->one,theta,epsilon);
    if (node->two !=NULL)
      F2=tree_force(p,node->two,theta,epsilon);
    if (node->three !=NULL)
      F3=tree_force(p,node->three,theta,epsilon);
    if (node->four !=NULL)
      F4=tree_force(p,node->four,theta,epsilon);
    if (node->five !=NULL)
      F5=tree_force(p,node->five,theta,epsilon);
    if (node->six !=NULL)
      F6=tree_force(p,node->six,theta,epsilon);
    if (node->seven !=NULL)
      F7=tree_force(p,node->seven,theta,epsilon);
    if (node->eight !=NULL)
      F8=tree_force(p,node->eight,theta,epsilon);
    
    F.x=F1.x+F2.x+F3.x+F4.x+F5.x+F6.x+F7.x+F8.x;
    F.y=F1.y+F2.y+F3.y+F4.y+F5.y+F6.y+F7.y+F8.y;
    F.z=F1.z+F2.z+F3.z+F4.z+F5.z+F6.z+F7.z+F8.z;
    
    
    return F;
}
}



int condition(particle_t *p,tnode_t *node,double theta) /// revisar condicion supone un arbol cuadrado
{
  if (((node->x_max - node->x_min)*(node->x_max - node->x_min)) < (distance_square(p,node)*theta*theta))
    return(1);
  else
    return(0);
      
}

double distance_square(particle_t *p,tnode_t *node)
{

  return (node->x-p->x)*(node->x-p->x)+(node->y-p->y)*(node->y-p->y)+(node->z-p->z)*(node->z-p->z);

}

double distance_square_particle_particle(particle_t *p,particle_t *node)
{

  return (node->x-p->x)*(node->x-p->x)+(node->y-p->y)*(node->y-p->y)+(node->z-p->z)*(node->z-p->z);

}

void print_to_a_file(FILE *fp,particle_t *P,int step,int N)
{
  int i;
  fprintf(fp,"\n \n  # %d step \n",step);
  for(i=0;i<N;i++){
    //        printf("%f %f\n",*(c_real+i),*(c_img+j));
    fprintf(fp,"%f %f %f\n",(P+i)->x,(P+i)->y,(P+i)->z);
}
}


graphs_t get_graphs(int N_particles,double theta,double epsilon){
  
  particle_t *p,*p2;
  force_t *F,*F2,*FORCE_cmp;
  tnode_t *root;
  graphs_t graph;
  clock_t t;
  int i,j;
  double time_taken,tpp,ttree,t_creation_tree,error;//pp,time_taketree;
  
  p = (particle_t *) calloc(N_particles, sizeof(particle_t));
  p2 = (particle_t *) calloc(1, sizeof(particle_t));
  FORCE_cmp=( force_t *) calloc(N_particles, sizeof(force_t));
  F=( force_t *) calloc(N_particles, sizeof(force_t));
  F2=( force_t *) calloc(N_particles, sizeof(force_t));
  // make initial conditions//
  for(i=0;i<N_particles;i++){
    (p+i)->x=drand48();
    (p+i)->y=drand48();
    (p+i)->z=drand48();
    (p+i)->m=(double)1.0;
  }

  // create a tree
  t = clock();
  root=create_a_tree(p,p2,N_particles);
  t = clock() - t;
  time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
  t_creation_tree=time_taken;
  tpp=0.,ttree=0.,error=0.;
  for(i=0;i<N_particles;i++)
    {
      t = clock();
      F[i]=tree_force(p+i,root,theta,epsilon);
      t = clock() - t;
      time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
      ttree+=time_taken;
      
      t = clock();
      F2[i]=particle_particle_force(p+i,p,N_particles,epsilon);
      t = clock() - t;
      time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
      tpp+=time_taken;

      (FORCE_cmp+i)->x=fabs((F[i].x-F2[i].x)/F[i].x);
      (FORCE_cmp+i)->y=fabs((F[i].y-F2[i].y)/F[i].y);
      (FORCE_cmp+i)->z=fabs((F[i].z-F2[i].z)/F[i].z);
      //error+=(FORCE_cmp+i)->x+(FORCE_cmp+i)->y+(FORCE_cmp+i)->z;
      error+=fabs(1.-sqrt((F2[i].x)*(F2[i].x)+(F2[i].y)*(F2[i].y)+(F2[i].z)*(F2[i].z))/sqrt((F[i].x)*(F[i].x)+(F[i].y)*(F[i].y)+(F[i].z)*(F[i].z)));
    }
  error=error/(N_particles);
  graph.tpp=tpp;
  graph.ttree=ttree;
  graph.t_creation_tree=t_creation_tree;
  graph.error=error;
  
  free_tree(root);
  free(FORCE_cmp);
  free(F2);
  free(F);
  free(p);
  free(p2);
  return graph;
}


