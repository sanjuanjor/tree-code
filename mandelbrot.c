#include <stdio.h>
#include <complex.h>
#include <math.h>
#include <tgmath.h>
#include <omp.h>
#include <time.h>

#define n 2000 //dimensions of the cell
#define minimg -1.1
#define maximg 1.1
#define minreal -2.
#define maxreal 1.1
#define nmaxinteration 1000


int main(){

  time_t t0,t1;
  double dt;
  omp_set_num_threads(4);
  //register int i,j,count;
  int i,j,count;
  //register double complex z,c;
  double complex z,c;
  double realstep=(maxreal-minreal)/n;
  double imgstep=(maximg-minimg)/n;
  // = 1.0 + 1.0*I;
  //  printf("%.1f%+.1fi cartesian is rho=%f theta=%f polar\n", creal(z), cimag(z), cabs(z), carg(z));
    //  calloc a 2D array
  //  x=(double*)calloc(n,sizeof(double));
  //  y=(double*)calloc(n,sizeof(double));

t0=time(NULL);
#pragma omp parallel for private(i,j,count,z,c)
  for(i=0;i<n;i++){
    for (j=0;j<n;j++){
      count=0;
      c=minreal+realstep*i+(minimg+imgstep*j)*I;
      z=0.+0.*I;
      while(count<nmaxinteration && cabs(z)<2){
        count++;
        z=z*z+c;
      }
      if(count==nmaxinteration){
        printf("%f %f\n",minreal+realstep*i,minimg+imgstep*j);
        }
    }
  }

  t1=time(NULL);
  dt=difftime(t1,t0);
  fprintf(stderr,"%f \n",dt);
  return(0);
}
