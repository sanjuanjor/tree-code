#include <math.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include "immintrin.h"
#include <stdio.h>
#include <inttypes.h>
// we are going to chek the int declarations;
void main(void)
{
  fprintf(stderr,"int: %ld\n",sizeof(int));
  fprintf(stderr,"unsinged int: %ld\n",sizeof(unsigned int));
  fprintf(stderr,"long: %ld\n",sizeof(long));
  fprintf(stderr,"unsinged long: %ld\n",sizeof(unsigned long));
  fprintf(stderr,"unsinged long long: %ld\n",sizeof(long long));
  fprintf(stderr,"unsinged long long: %ld\n",sizeof(unsigned long long));
  // this are machine dependt for thaht we use std int
  int32_t i32;
  __int128_t i128;
  __uint128_t iu128;
  __int256_t i256;
  __m256i m256i;
  __m512i m512i;
  uint32_t ui32;
  int64_t i64;
  uint64_t ui64;
  int16_t i_16;
  int8_t i8;
  uint16_t ui16;
  uint8_t ui8;
  
  int8_t *ia8;
  
  ia8 =calloc(10,sizeof(int8_t));
  ia8[0]=12;
  ia8[9]=27;
  fprintf(stderr,"ia8: %ld\n\n",sizeof(ia8));// its the size qof a pointer 

  fprintf(stderr,"int8:       %ld\n",sizeof(int8_t));
  fprintf(stderr,"uint8:      %ld\n",sizeof(uint8_t));
  fprintf(stderr,"int16:      %ld\n",sizeof(int16_t));  
  fprintf(stderr,"uint16:     %ld\n",sizeof(uint16_t));
  fprintf(stderr,"int32:      %ld\n",sizeof(int32_t));
  fprintf(stderr,"uint3:      %ld\n",sizeof(uint32_t));
  fprintf(stderr,"int64:      %ld\n",sizeof(int64_t));
  fprintf(stderr,"uint64      %ld\n",sizeof(uint64_t));
  fprintf(stderr,"uint128:    %ld\n",sizeof(__uint128_t));               
  fprintf(stderr,"int128:     %ld\n",sizeof(__int128_t));
  fprintf(stderr,"m256i:      %ld\n",sizeof(__m256i));
  fprintf(stderr,"m512i:      %ld\n",sizeof(__m512i));
  fprintf(stderr,"m256i:      %ld\n",sizeof(__int256_t));
  i128=(__int128_t)1;
  i128++;
  fprintf(stderr,"m512i works??:      %ld\n",i128);
}
